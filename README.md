# Net5_TestApp

Run following commands to pull, build, run and push the app.

`docker login registry.gitlab.com`

`docker pull registry.gitlab.com/k1203/net5_testapp`

`docker build -t registry.gitlab.com/k1203/net5_testapp --build-arg BASE_PATH=<base_path> .`

`docker run -d -p 8080:80  registry.gitlab.com/k1203/net5_testapp `

`docker push registry.gitlab.com/k1203/net5_testapp`